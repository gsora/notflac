package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/dhowden/tag"
)

var (
	baseDir string
	al      AlbumList
	verbose bool
)

type Album struct {
	Name   string
	Artist string
}

type AlbumList struct {
	List map[Album]int
}

func main() {
	flag.StringVar(&baseDir, "path", "", "base path")
	flag.BoolVar(&verbose, "verbose", false, "list all work-in-progress files")
	flag.Parse()

	al = NewAlbumList()
	err := filepath.Walk(baseDir, walkingFunc)

	if err != nil {
		fmt.Printf("error walking the path %q: %v\n", baseDir, err)
	}

	for k, _ := range al.List {
		fmt.Println(k.Artist, "-", k.Name)
	}
}

func walkingFunc(path string, info os.FileInfo, err error) error {
	if err != nil {
		fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
		return err
	}

	if info.IsDir() {
		return nil
	}

	f, err := os.Open(path)
	defer f.Close()
	if err != nil {
		log.Println(err)
		return nil
	}

	m, err := tag.ReadFrom(f)
	if err != nil {
		return nil
	}

	if m.FileType() == tag.FLAC {
		return nil
	}

	if verbose {
		log.Println("processing file", info.Name())
	}

	artist := m.Artist()
	album := m.Album()
	if m.AlbumArtist() != "" {
		artist = m.AlbumArtist()
	}

	if album == "" {
		album = path
		artist = "Missing metadata"
	}
	al.Add(album, artist)

	return nil
}

func NewAlbumList() (a AlbumList) {
	a.List = make(map[Album]int)
	return a
}

func (a *AlbumList) Add(album string, artist string) {
	a.List[Album{album, artist}] = 1
}
