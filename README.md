# notflac

Scan a folder for non-FLAC albums.

## Building

This repository is go get-able:

```
go get gitlab.com/gsora/notflac
```

## Running

```
notflac -path yourpath
```

Use the parameter `-verbose` to see which file `notflac` is working on.
